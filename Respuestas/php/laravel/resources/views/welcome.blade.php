<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>

    <div class="w-3/5 mx-auto my-6">

        <div class="my-6">
            <a href="https://itnazca.com/" target='_blank'>
                <img src="https://itnazca.com/images/logo.png" alt="" class="my-6">
            </a>
            <h1 class="text-3xl font-bold my-3 text-lime-700">Prueba de Desarrollo: Laravel</h1>
            <h2 class="text-xl">01 - CRUD</h2>
        </div>

        <div class="bg-lime-200 p-7 mt-2">
        
            <div class="bg-black text-white p-3">
                <p class="my-2 font-bold text-lg text-yellow-300">Contactos</p>
            </div>

            <div class="mt-10">
            
            <a href="" class='bg-lime-900 hover:bg-lime-500 hover:text-lime-900 text-white px-4 py-3 rounded-lg'>Agregar Contacto</a>
            
            <div class="relative overflow-x-auto mt-10">
                
            </div>

                
            </div>

        </div>

        <div class="mt-8">
            <p class="text-slate-400">@julioIzquierdoMejia</p>
        </div>

    </div>

    
  
</body>
</html>