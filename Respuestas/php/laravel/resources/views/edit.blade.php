<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>

    <div class="w-3/5 mx-auto my-6">

        <div class="my-6">
            <a href="https://itnazca.com/" target='_blank'>
                <img src="https://itnazca.com/images/logo.png" alt="" class="my-6">
            </a>
            <h1 class="text-3xl font-bold my-3 text-lime-700">Prueba de Desarrollo: Laravel</h1>
            <h2 class="text-xl">01 - CRUD</h2>
        </div>

        <div class="bg-lime-200 p-7 mt-2">
        
            <div class="bg-black text-white p-3">
                <p class="my-2 font-bold text-lg text-yellow-300">Crear Contactos</p>
            </div>

            <div class="mt-10">
            
            <a href="{{ route('contactos.index') }}" class='bg-lime-900 hover:bg-lime-500 hover:text-lime-900 text-white px-4 py-3 rounded-lg'>Ver lista de contactos</a>

            
            <div class="relative overflow-x-auto mt-10">
            
            <form action="{{ route('contactos.update',$contacto->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="grid grid-cols-4 gap-4">

                <div class='group-fields my-2 col-span-4'>
                    <label for="Nombre" class='py-3'>Nombre</label>
                    <div>
                        <input type="text" class='px-4 py-2 mt-3 rounded-lg w-full' placeholder='Ingresar Nomnbre' name='name' value="{{ $contacto->name }}">

                        @error('name')
                        <div class="bg-red-500 px-3 py-2 text-white text-sm mt-2 rounded-lg">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class='group-fields my-2 col-span-4'>
                    <label for="Nombre" class='py-3'>Correo</label>
                    <div>
                        <input type="text" class='px-4 py-2 mt-3 rounded-lg w-full' placeholder='Ingresar Correo' name='email' value="{{ $contacto->email }}">
                        @error('email')
                        <div class="bg-red-500 px-3 py-2 text-white text-sm mt-2 rounded-lg">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class='group-fields my-2 col-span-2'>
                    <label for="Nombre" class='py-3'>Teléfono</label>
                    <div>
                        <input type="text" class='px-4 py-2 mt-3 rounded-lg w-full' placeholder='Ingresar Teléfono' name='phone' value="{{ $contacto->phone }}">
                        @error('phone')
                        <div class="bg-red-500 px-3 py-2 text-white text-sm mt-2 rounded-lg">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div class='group-fields my-2 col-span-2'>
                    <label for="Nombre" class='py-3'>Edad</label>
                    <div>
                        <input type="text" class='px-4 py-2 mt-3 rounded-lg w-full' placeholder='Ingrese Edad' name='age' value="{{ $contacto->age }}">
                        @error('age')
                        <div class="bg-red-500 px-3 py-2 text-white text-sm mt-2 rounded-lg">{{ $message }}</div>
                        @enderror
                    </div>
                </div>

                <div>
                    <button type="submit" class="bg-lime-900 hover:bg-lime-500 hover:text-lime-900 text-white px-4 py-3 rounded-lg justify-items-center">Edit</button>    
                </div>
                

                </div>

            </form>
            

            </div>

                
            </div>

        </div>

        <div class="mt-8">
            <p class="text-slate-400">@julioIzquierdoMejia</p>
        </div>

    </div>

    
  
</body>
</html>