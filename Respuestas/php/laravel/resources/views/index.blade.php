<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
<link rel="stylesheet" href="sweetalert2.min.css">
</head>
<body>

    <div class="w-3/5 mx-auto my-6">

        <div class="my-6">
            <a href="https://itnazca.com/" target='_blank'>
                <img src="https://itnazca.com/images/logo.png" alt="" class="my-6">
            </a>
            <h1 class="text-3xl font-bold my-3 text-lime-700">Prueba de Desarrollo: Laravel</h1>
            <h2 class="text-xl">01 - CRUD</h2>
        </div>

        <div class="bg-lime-200 p-7 mt-2">
        
            <div class="bg-black text-white p-3">
                <p class="my-2 font-bold text-lg text-yellow-300">Contactos</p>

                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                
            </div>

            <div class="mt-10">
            
            <a href="{{ route('contactos.create') }}" class='bg-lime-900 hover:bg-lime-500 hover:text-lime-900 text-white px-4 py-3 rounded-lg'>Agregar Contacto</a>

            
            <div class="relative overflow-x-auto mt-10">
                <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                        <tr>
                            <th scope="col" class="px-6 py-3">
                                Nombre
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Correo
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Telefono
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Edad
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Estado
                            </th>
                            <th scope="col" class="px-6 py-3">
                                Acciones
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($contactos as $contacto)
                        
                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                            <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                {{ $contacto->name }}
                            </th>
                            <td class="px-6 py-4">
                                {{ $contacto->email }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $contacto->phone }}
                            </td>
                            <td class="px-6 py-4">
                                {{ $contacto->age }}
                            </td>
                            <td class="px-6 py-4">
                                @if($contacto->active == 1)
                                    <span>Activo</span>
                                @else
                                    <span>Inactivo</span>
                                @endIf
                            </td>
                            <td class="px-6 py-4">
                                <form action="{{ route('contactos.destroy',$contacto->id) }}" method="Post">
                                    <a class="bg-blue-500 text-white px-3 py-2 text-dm" href="{{ route('contactos.edit',$contacto->id) }}">Edit</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="bg-red-500 text-white px-3 py-2 text-dm">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endForEach
                    </tbody>
                </table>
            </div>

                
            </div>

        </div>

        <div class="mt-8">
            <p class="text-slate-400">@julioIzquierdoMejia</p>
        </div>

    </div>
    
</body>
</html>