<?php

namespace App\Http\Controllers;
use App\Models\Contactos;
use Illuminate\Http\Request;

class ContactosController extends Controller
{
    //
    public function index()
    {
        $contactos = Contactos::all();
        return view('index', compact('contactos'));
    }


    public function create()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'age' => 'required',
        ]);
        
        Contactos::create($request->post());

        return redirect()->route('contactos.index')->with('success','Se ha creado satifactoriamente el contacto '. $request->name);
    }

    public function edit(Contactos $contacto)
    {
        return view('edit',compact('contacto'));
    }


    public function update(Request $request, Contactos $contacto)
    {
        
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'age' => 'required',
        ]);
        
        $contacto->fill($request->post())->save();

        return redirect()->route('contactos.index')->with('success','el contacto '. $contacto->name .' ha sido actualizado');
    }

    public function destroy(Contactos $contacto)
    {
        $contacto->delete();
        return redirect()->route('contactos.index')->with('success','El contacto '. $contacto->name .' ha sido eliminado');
    }
}
