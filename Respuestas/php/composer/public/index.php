<?php 

//require_once '/vendor/autoload';
require dirname(__DIR__) .  '/vendor/autoload.php';
use Carbon\Carbon;

//Metodno para imprimir lista de zonas horarias
//echo "<pre>";
//var_dump(DateTimezone::listIdentifiers());

//configuramos la Zona horaria
date_default_timezone_set('America/Lima');

//configurar el idioma para el formato de las fechas en carbon 
Carbon::setlocale('es');

//iniciamos con la libreria

//si queremos pasarle un dato de fecha a carbon de una DB lo hacemos con el constructor

$fecha = new Carbon('1976-11-14 10:30:00');

$cantidadDias = $fecha->diffInDays(Carbon::now());


?>

<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>

    <div class="w-3/5 mx-auto my-6">

        <div class="my-6">
            <a href="https://itnazca.com/" target='_blank'>
                <img src="https://itnazca.com/images/logo.png" alt="" class="my-6">
            </a>
            <h1 class="text-3xl font-bold my-3 text-lime-700">Prueba de Desarrollo: Composer</h1>    
            <h2 class="text-xl">01 - Librería CARBON</h2>
        </div>

        <div class="bg-lime-200 p-7 mt-2">
        
            <div class="bg-black text-white p-3">
                <p>Fecha de mi Nacimeinto</p>
                <p class="my-2 font-bold text-xl text-yellow-500"><?php echo $fecha; ?></p>
            </div>

            <div class="grid grid-cols-4 gap-4 mt-10">
                
                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>La fecha Actual</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo Carbon::now(); ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Agregar una semana</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo Carbon::now()->addWeek(); ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Restar 3 meses</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo Carbon::now()->subMonths(3); ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Formato Mes dia Año</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo Carbon::now()->format('m-d-Y'); ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Dias desde que Naci hasta hoy</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $cantidadDias; ?></p>
                </div>

                
            </div>

        </div>

        <div class="mt-8">
            <p class="text-slate-400">@julioIzquierdoMejia</p>
        </div>

    </div>

    
  
</body>
</html>