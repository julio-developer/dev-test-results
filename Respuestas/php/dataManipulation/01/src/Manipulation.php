<?php

namespace Test\Manipulation01;

class Manipulation
{

    public function entero($numero)
    {
        //return intval($numero);
        return floor($numero);
    }

    public function decimal($numero)
    {
        $entero = floor($numero);
        return $numero - $entero;
    }

    public function redondear($numero)
    {
        return round($numero);
    }

    public function redondear_inferior($numero)
    {
        return floor($numero);
    }

    public function redondear_superior($numero)
    {
        return ceil($numero);
    }

    public function formatNumber($number)
    {
        return number_format($number, 2);
    }

}