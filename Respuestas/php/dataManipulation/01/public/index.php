<?php 

require dirname(__DIR__) .  '/vendor/autoload.php';


use Test\Manipulation01\Manipulation;
$math = new Manipulation();

//valor a manipular
$numero = 1324223.1365593;

$redondeado = $math->redondear($numero);
$entero = $math->entero($numero);
$decimal = $math->decimal($numero);
$redondedoInferior = $math->redondear_inferior($numero);
$redondedoSuperior = $math->redondear_inferior($numero);
$numerFormat = $math->formatNumber($numero);

?>
<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>

    <div class="w-3/5 mx-auto my-6">

        <div class="my-6">
            <a href="https://itnazca.com/" target='_blank'>
                <img src="https://itnazca.com/images/logo.png" alt="" class="my-6">
            </a>
            <h1 class="text-3xl font-bold my-3 text-lime-700">Prueba de Desarrollo: Data Manipulation</h1>    
            <h2 class="text-xl">01 - Números</h2>
        </div>

        <div class="bg-lime-200 p-7 mt-2">
        
            <div class="bg-black text-white p-3">
                <p>Valor a Manipular</p>
                <p class="my-2 font-bold text-xl text-yellow-500"><?php echo $numero ?></p>
            </div>

            <div class="grid grid-cols-4 gap-4 mt-10">
                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Redondeado</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $redondeado ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Entero</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $entero ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Decimal</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $decimal ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Redondedo Superior</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $redondedoSuperior ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Redondedo Inferior</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $redondedoInferior ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Fromateado</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $numerFormat ?></p>
                </div>
            </div>

        </div>

        <div class="mt-8">
            <p class="text-slate-400">@julioIzquierdoMejia</p>
        </div>

    </div>

    
  
</body>
</html>