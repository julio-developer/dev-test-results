<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5374c201214e3e75c94747a36fa142e7
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Test\\Manipulation02\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Test\\Manipulation02\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit5374c201214e3e75c94747a36fa142e7::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit5374c201214e3e75c94747a36fa142e7::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit5374c201214e3e75c94747a36fa142e7::$classMap;

        }, null, ClassLoader::class);
    }
}
