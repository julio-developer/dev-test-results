<?php

namespace Test\Manipulation02;

class Cadena
{

    public function mayusculas($cadena)
    {
        //return intval($numero);
        return strtoupper($cadena);
    }

    public function contar($cadena)
    {
        $cantidad = strlen($cadena);
        return $cantidad;
    }

    public function contarLetras($cadena)
    {
        $sinespacios = trim($cadena);//eliminamos posibles espaciones en blanco al inicio y al final 
        $soloLetras =str_replace(' ', '', $sinespacios);
        $cantidad = strlen($soloLetras);
        return $cantidad;
    }


    function contarMayusculas($cadena){
        $contador=0;
        for($v=0;$v<strlen($cadena);$v++){
            for($i=65;$i<=90;$i++){
                if($cadena[$v]==chr($i)){
                    $contador++;
                }
            }
        }
        
        return $contador;
    }

    function camel_case($cadena) {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $cadena, $concurrencias);
        $ret = $concurrencias[0];
        foreach ($ret as &$match) {
          $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
      }

    

}