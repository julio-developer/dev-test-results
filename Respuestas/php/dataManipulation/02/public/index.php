<?php 

require dirname(__DIR__) .  '/vendor/autoload.php';


use Test\Manipulation02\Cadena;
$cadena = "Hola Mundo   ";

use Test\Manipulation02\Manipulation;
$cadenas = new Cadena;

$cantidad = $cadenas->contar($cadena);
$cantidadLetras = $cadenas->contarLetras($cadena);
$cantidadMayusculas = $cadenas->contarMayusculas($cadena);

$snake_case = $cadenas->camel_case($cadena);


?>
<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>

    <div class="w-3/5 mx-auto my-6">

        <div class="my-6">
            <a href="https://itnazca.com/" target='_blank'>
                <img src="https://itnazca.com/images/logo.png" alt="" class="my-6">
            </a>
            <h1 class="text-3xl font-bold my-3 text-lime-700">Prueba de Desarrollo: Data Manipulation</h1>    
            <h2 class="text-xl">02 - Cadenas</h2>
        </div>

        <div class="bg-lime-200 p-7 mt-2">
        
            <div class="bg-black text-white p-3">
                <p>Valor a Manipular</p>
                <p class="my-2 font-bold text-xl text-yellow-500"><?php echo $cadena ?></p>
            </div>

            <div class="grid grid-cols-4 gap-4 mt-10">
                
            <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Contando Caracteres</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $cantidad ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Contando solo Letras</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $cantidadLetras ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Contando Mayúsculas</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $cantidadMayusculas ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Pasar a Snake Case</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $snake_case ?></p>
                </div>

            </div>

        </div>

        <div class="mt-8">
            <p class="text-slate-400">@julioIzquierdoMejia</p>
        </div>

    </div>

    
  
</body>
</html>