<?php 

namespace Test\Manipulation01;

Class Operations extends Numbers
{

    static public function multi($a, $b)
    {
        return $a * $b;
    }

    static public function div($a, $b)
    {
        return $a / $b;
    }

}