<?php 

namespace Test\Manipulation01;

class Numbers
{

    static public function sum($a, $b)
    {
        return $a + $b;
    }

    static public function diff($a, $b)
    {
        return $a - $b;
    }

    static public function format($number)
    {
        $number_format = number_format($number, 2, '.', ',');

        return $number_format;
    }

    static public function format4($number, $d)
    {
        $number_format = number_format($number, $d, '.', ',');

        return $number_format;
    }



}
