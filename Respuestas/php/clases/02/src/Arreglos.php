<?php

namespace Test\Manipulation01;

class Arreglos
{

    public function acumulado($arreglo)
    {
        $acumulado = array_reduce( $arreglo, function($total, $current) { return $total + $current; } );
        return ($acumulado);
    }


    public function impares($arreglo)
    {
        $impares = array_filter($arreglo, function($data){
            return $data & 1;
        });
        
        $sumaImpares = array_reduce( $impares, function($total, $current) { return $total + $current; } );

        return $sumaImpares;
    }

    public function reverse($arreglo)
    {

        $invertido = array_reverse($arreglo);
        $json = json_encode($invertido); 
        return $json;

    }

    

}