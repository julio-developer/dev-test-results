<?php 


namespace Test\Manipulation01;

class Numbers
{

    public function sum($a, $b)
    {
        return $a + $b;
    }

    public function diff($a, $b)
    {
        return $a - $b;
    }

    public function format($number)
    {
        $number_format = number_format($number, 2, '.', ',');

        return $number_format;
    }

    public function format4($number, $d)
    {
        $number_format = number_format($number, $d, '.', ',');

        return $number_format;
    }

    public function formatMoney($number)
    {
        $fmt = new NumberFormatter( 'de_DE', NumberFormatter::CURRENCY );
        echo $fmt->formatCurrency(1234567.891234567890000, "EUR")."\n";
        echo $fmt->formatCurrency(1234567.891234567890000, "RUR")."\n";
        $fmt = new NumberFormatter( 'ru_RU', NumberFormatter::CURRENCY );
        echo $fmt->formatCurrency(1234567.891234567890000, "EUR")."\n";
        echo $fmt->formatCurrency(1234567.891234567890000, "RUR")."\n";

    }


}
