<?php 

namespace Test\Manipulation01;

class NumbersConfig
{

    static public function formatoNumber(float $number, int $decimal=2, String $separadorCientos, String $separadorMiiles, String $currecny='')
    {  
        $number_format = number_format($number, $decimal, $separadorCientos, $separadorMiiles);
        return $currecny.$number_format;
    }


}
