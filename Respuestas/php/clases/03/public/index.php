<?php 

require dirname(__DIR__) .  '/vendor/autoload.php';

use Test\Manipulation01\NumbersConfig;
$numeroBase = 1262.25361154;
$number = new NumbersConfig();
$formato1 = $number::formatoNumber($numeroBase, 2, ',', ' ');
$formato2 = $number::formatoNumber($numeroBase, 4, ',', ' ');
$formatoMoney = $number::formatoNumber($numeroBase, 2, ',', ' ', 'US$ ');


?>
<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>

    <div class="w-3/5 mx-auto my-6">

        <div class="my-6">
            <a href="https://itnazca.com/" target='_blank'>
                <img src="https://itnazca.com/images/logo.png" alt="" class="my-6">
            </a>
            <h1 class="text-3xl font-bold my-3 text-lime-700">Prueba de Desarrollo: Class (Classes)</h1>    
            <h2 class="text-xl">01 - Bases</h2>
        </div>

        <div class="bg-lime-200 p-7 mt-2">
        
            <div class="bg-black text-white p-3">
                <p>Valor a Manipular</p>
                <p class="my-2 font-bold text-xl text-yellow-500"><?php echo $numeroBase ?></p>
            </div>

            <div class="grid grid-cols-4 gap-4 mt-10">
                
            <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>formato 1 - 2 deciamles</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $formato1 ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                <p>formato 1 - 4 deciamles</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $formato2 ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>formato Money</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $formatoMoney ?></p>
                </div>

                
            </div>

        </div>

        <div class="mt-8">
            <p class="text-slate-400">@julioIzquierdoMejia</p>
        </div>

    </div>

    
  
</body>
</html>