<?php 

require dirname(__DIR__) .  '/vendor/autoload.php';


$arreglo = array(1,2,3,4,5,6,7);

use Test\Manipulation01\Arreglos;
$arreglos = new Arreglos();

$acumulado = $arreglos->acumulado($arreglo);
$sumaImpares = $arreglos->impares($arreglo);
$reverse = $arreglos->reverse($arreglo);


?>
<!doctype html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <script src="https://cdn.tailwindcss.com"></script>
</head>
<body>

    <div class="w-3/5 mx-auto my-6">

        <div class="my-6">
            <a href="https://itnazca.com/" target='_blank'>
                <img src="https://itnazca.com/images/logo.png" alt="" class="my-6">
            </a>
            <h1 class="text-3xl font-bold my-3 text-lime-700">Prueba de Desarrollo: Arrays</h1>    
            <h2 class="text-xl">01 - Operaciones Lineales</h2>
        </div>

        <div class="bg-lime-200 p-7 mt-2">
        
            <div class="bg-black text-white p-3">
                <p>Valor a Manipular</p>
                <p class="my-2 font-bold text-xl text-yellow-500"><?php echo "array=[1,2,3,4,5,6,7]" ?></p>
            </div>

            <div class="grid grid-cols-4 gap-4 mt-10">
                
                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Acumulado</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $acumulado ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Suma de impares</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $sumaImpares ?></p>
                </div>

                <div class="bg-lime-800 text-white p-3 rounded-xl">
                    <p>Array Invertido</p>
                    <p class="my-2 font-bold text-lg text-yellow-300"><?php echo $reverse ?></p>
                </div>

                
            </div>

        </div>

        <div class="mt-8">
            <p class="text-slate-400">@julioIzquierdoMejia</p>
        </div>

    </div>

    
  
</body>
</html>