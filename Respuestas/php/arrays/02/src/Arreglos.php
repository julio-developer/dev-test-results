<?php

namespace Test\Manipulation01;

class Arreglos
{

    public function acumuladoEdad($matrix)
    {        
        $edades = array_reduce($matrix, function ($acumulado, $item)
        {
            $acumulado += $item['age'];
            return $acumulado;
        });
        
        return $edades;

        //print_r($red);
    }


    public function suma30($arreglo)
    {
        $r = array_filter( $arreglo, function( $e ) {
            return $e['age'] > 30;
        });

        //print_r($r);
        $suma30 = array_reduce($r, function ($acumulado, $item)
        {
            $acumulado += $item['age'];
            return $acumulado;
        });
        
        return $suma30;
    }

    public function reverse($arreglo)
    {

        $invertido = array_reverse($arreglo);
        $json = json_encode($invertido); 
        return $json;

    }

    public function adulto($arreglo)
    {
        $matrix_calc = array_reduce($arreglo, function ($matrix_f, $item)
        {
            $item['adult'] = $item['age'] > 17 ? 'Adulto' : 'Menor de Edad';
            $matrix_f[] = $item;
            
            return $matrix_f;
        });
        
        $json = json_encode($matrix_calc);
        return $json;
    }

    

}